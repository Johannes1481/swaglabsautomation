package SwagLabs;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByClassName;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.server.handler.FindElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
@Test
public class navigateSwagLabs {
	
	//decalring private webdriver to navigateSwagLabs class
	private WebDriver driver;
	Thread thread = new Thread();
	SwagLabsMethods swaglabsmethods = new SwagLabsMethods();
	
	@Test
	//initialize the Webdriver driver
	public void beforeTest()
	{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\USER\\My_Projects\\GlobalKinetic\\src\\test\\drivers\\chromedriver.exe");
		driver = new ChromeDriver(); 
		 driver.manage().window().maximize();
		driver.get("https://www.saucedemo.com/");
		
	}
	
	@Test
	public void login() throws IOException, InterruptedException
	{
		String userNames = driver.findElement(By.xpath("//*[@id=\"login_credentials\"]")).getText().toString();
		swaglabsmethods.writeToFileUsername(userNames);
		System.out.println(swaglabsmethods.readUserName()); 
		
		String password = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]")).getText().toString();
		swaglabsmethods.writeToFilePassword(password);
		System.out.println(swaglabsmethods.readPassword());
				
		//filling in the login details - username
		driver.findElement(By.xpath("//*[@id=\"user-name\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"user-name\"]")).sendKeys(swaglabsmethods.readUserName());
		
		//filling in the details for password
		driver.findElement(By.xpath("//*[@id=\"password\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys(swaglabsmethods.readPassword());
		
		//Clicking the login button
		driver.findElement(By.xpath("//*[@id=\"login-button\"]")).click();
		
	}
	
	@Test
	public void loginHomePageAddItemsOnCart() throws InterruptedException
	{
		
		thread.sleep(3000);
		//adding items to the card
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[2]/div/div/div/div[1]/div[2]/div[1]/a/div")).click();
		driver.findElement(By.xpath("//*[@id=\"add-to-cart-sauce-labs-backpack\"]")).click();
		
		thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"back-to-products\"]")).click();
		
		thread.sleep(3000);
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[2]/div/div/div/div[6]/div[2]/div[1]/a/div")).click();
		driver.findElement(By.xpath("//*[@id=\"add-to-cart-test.allthethings()-t-shirt-(red)\"]")).click();
		
		thread.sleep(3000);
		
		//Clickin back to products button
		driver.findElement(By.xpath("//*[@id=\"back-to-products\"]")).click();
		thread.sleep(3000);
		
		//Checking out the cart
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]")).click();
		driver.findElement(By.xpath("//*[@id=\"checkout\"]")).click();
		
	}
	
	@Test
	public void populateYourInformation() throws InterruptedException
	{
		
		String[] personalDetails = (String[]) swaglabsmethods.yourInformation();
		String firstName = personalDetails[0];
		String lastName = personalDetails[1];
		String postCode = personalDetails[2];
		
		System.out.println("first name :"+firstName+" : lastname : "+lastName+" postcode :"+postCode);
		
		//populating your information details
		String firstNameXpath = "//*[@id=\"first-name\"]";
		driver.findElement(By.xpath(firstNameXpath)).click();
		driver.findElement(By.xpath(firstNameXpath)).sendKeys(firstName);
		
		String lastNameXpath = "//*[@id=\"last-name\"]";
		driver.findElement(By.xpath(lastNameXpath)).click();
		driver.findElement(By.xpath(lastNameXpath)).sendKeys(lastName);
		
		String postCodeXpath = "//*[@id=\"postal-code\"]";
		driver.findElement(By.xpath(postCodeXpath)).click();
		driver.findElement(By.xpath(postCodeXpath)).sendKeys(postCode);
		
		thread.sleep(3000);
		//clicking CONTINUE button
		String continueXpath = "//*[@id=\"continue\"]";
		driver.findElement(By.xpath(continueXpath)).click();
		
		//Asserting that 
		String assertCheckoutXpath = "/html/body/div[1]/div/div/div[1]/div[2]/span";
		driver.findElement(By.xpath(assertCheckoutXpath)).isDisplayed();
		
		assertCheckoutXpath = driver.findElement(By.xpath(assertCheckoutXpath)).getText();
		System.out.println("the string is : "+assertCheckoutXpath);
		assertEquals("CHECKOUT: OVERVIEW", assertCheckoutXpath);
		
		//Check if the quantity is morethan zero
		String quantityXpath = "/html/body/div[1]/div/div/div[2]/div/div[1]/div[3]/div[1]";
		String quantity = driver.findElement(By.xpath(quantityXpath)).getText();
		assertNotEquals("0", quantity);
		
		//clicking FINISH button
		String finishXpath = "//*[@id=\"finish\"]";
		driver.findElement(By.xpath(finishXpath)).click();
			
	}

	//closing the driver instance after execution
	@AfterTest
	public void afterTest()
	{
		driver.close();
	}
}
