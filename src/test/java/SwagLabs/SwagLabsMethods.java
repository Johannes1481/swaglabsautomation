package SwagLabs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.testng.annotations.Test;

import net.bytebuddy.asm.Advice.Return;

public class SwagLabsMethods {
  @Test
  public void f() {
  }
  
  @Test
  public void writeToFileUsername(String content) throws IOException
  {
      FileWriter filewriterusername = new FileWriter("C:\\Users\\USER\\My_Projects\\GlobalKinetic\\src\\test\\credentials\\username.txt",false);
      PrintWriter out_username = new PrintWriter(filewriterusername);
    
      out_username.println(content);
      out_username.close();
  }
   //writing password from the application
  @Test
  public void writeToFilePassword(String content) throws IOException
  {
      FileWriter filewriterpassword = new FileWriter("C:\\Users\\USER\\My_Projects\\GlobalKinetic\\src\\test\\credentials\\password.txt",false);
      PrintWriter out_password = new PrintWriter(filewriterpassword);

      out_password.println(content);
      out_password.close();
  }
  
  //reading username 
  @Test
  public String readUserName() throws InterruptedException
  { 
	  Thread thread = new Thread();
	  thread.sleep(3000);
	        int n = 1; // The line number
	        String username = "";
	        try (Stream<String> lines = Files.lines(Paths.get("C:\\Users\\USER\\My_Projects\\GlobalKinetic\\src\\test\\credentials\\username.txt"))) {
	          username = lines.skip(n).findFirst().get();
       
	        }
	        catch(IOException e){
	          System.out.println(e);
	        }
	   return username;
  }
  
  //Reading password
  @Test
  public String readPassword() throws InterruptedException
  {
	  Thread thread1 = new Thread();
	  thread1.sleep(3000);
	  int n = 1; // The line number
      String password ="";
      try (Stream<String> lines = Files.lines(Paths.get("C:\\Users\\USER\\My_Projects\\GlobalKinetic\\src\\test\\credentials\\password.txt"))) {
        password = lines.skip(n).findFirst().get();
       }
      catch(IOException e){
        System.out.println(e);
      }
      
     return password;
  }
  
  public String[] yourInformation()
  {
	  String line ;  
	  String splitBy = ",";  
	  String[] personalDetails = null;
	  try   
	  {  
	  //parsing a CSV file into BufferedReader class constructor  
		  BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\USER\\My_Projects\\GlobalKinetic\\src\\test\\credentials\\yourInformation.csv"));  
		  while ((line = br.readLine()) != null)   //returns a Boolean value  
		  {  
			  personalDetails = line.split(splitBy);    // use comma as separator  
			 
		  }  
	  }   
	  catch (IOException e)   
	  {  
		  e.printStackTrace();  
	  }
	return personalDetails;  
   }  
	  
  }

